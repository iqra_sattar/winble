﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Windows.Devices.Bluetooth;
using Windows.Devices.Bluetooth.GenericAttributeProfile;
using Windows.Devices.Enumeration;

namespace BLEConsoleDLL
{
    public class BLECommunication : IBLE
    {

        #region Private Members
        static BluetoothLEDevice _selectedDevice = null;
        static BluetoothLEAttributeDisplay _selectedService = null;
        static BluetoothLEAttributeDisplay _selectedCharacteristic = null;
        static string _aqsAllBLEDevices = "(System.Devices.Aep.ProtocolId:=\"{bb7bb05e-5972-42b5-94fc-76eaa7084d49}\")";
        static string[] _requestedBLEProperties = { "System.Devices.Aep.DeviceAddress", "System.Devices.Aep.Bluetooth.Le.IsConnectable", };
        static DataFormat _dataFormat = DataFormat.UTF8;
        static List<BluetoothLEAttributeDisplay> _services = new List<BluetoothLEAttributeDisplay>();
        static List<BluetoothLEAttributeDisplay> _characteristics = new List<BluetoothLEAttributeDisplay>();
        static List<GattCharacteristic> _subscribers = new List<GattCharacteristic>();
        static ManualResetEvent _notifyCompleteEvent = new ManualResetEvent(false);
        static bool _primed = false;
        static TimeSpan _timeout = TimeSpan.FromSeconds(10);
        static string char_value = "";
        #endregion

        #region Properties
        public BluetoothLEDevice SelectedDevice { get => _selectedDevice; set => _selectedDevice = value; }
        public List<BluetoothLEAttributeDisplay> Services { get; set; }
        //public static List<BluetoothLEAttributeDisplay> Characteristics { get; set; }
        public  List<DeviceInformation> _deviceList = new List<DeviceInformation>();
        #endregion

        #region Constructor
        public BLECommunication()
        {
            Services = new List<BluetoothLEAttributeDisplay>();
           // Characteristics = new List<BluetoothLEAttributeDisplay>();
        } 
        #endregion

        #region Private Methods
        private async Task<int> GetGattServices()
        {
            var result = await _selectedDevice.GetGattServicesAsync(BluetoothCacheMode.Uncached);
            if (result.Status == GattCommunicationStatus.Success)
            {
                if (!Console.IsInputRedirected)
                    Console.WriteLine($"Found {result.Services.Count} services:");

                for (int i = 0; i < result.Services.Count; i++)
                {
                    var serviceToDisplay = new BluetoothLEAttributeDisplay(result.Services[i]);
                    Services.Add(serviceToDisplay);
                    _services.Add(serviceToDisplay);
                    Console.WriteLine($"Service name:  {serviceToDisplay.Name}");
                }
                Console.WriteLine($"Service Count:  {Services.Count}");
                return 1;
            }
            else
            {
                CloseDevice();
                return -1;
            }
        }
        private async Task<int> GetCharacteristics(string serviceName)
        {
            int retVal = 0;
            if (_selectedDevice != null)
            {
                if (!string.IsNullOrEmpty(serviceName))
                {
                    string foundName = Utilities.GetIdByNameOrNumber(_services, serviceName);

                    // If device is found, connect to device and enumerate all services
                    if (!string.IsNullOrEmpty(foundName))
                    {
                        var attr = _services.FirstOrDefault(s => s.Name.Equals(foundName));
                        IReadOnlyList<GattCharacteristic> characteristics = new List<GattCharacteristic>();

                        try
                        {
                            // Ensure we have access to the device.
                            var accessStatus = await attr.service.RequestAccessAsync();
                            if (accessStatus == DeviceAccessStatus.Allowed)
                            {
                                // BT_Code: Get all the child characteristics of a service. Use the cache mode to specify uncached characterstics only 
                                // and the new Async functions to get the characteristics of unpaired devices as well. 
                                var result = await attr.service.GetCharacteristicsAsync(BluetoothCacheMode.Uncached);
                                if (result.Status == GattCommunicationStatus.Success)
                                {
                                    characteristics = result.Characteristics;
                                    _selectedService = attr;
                                    _characteristics.Clear();
                                    if (!Console.IsInputRedirected) Console.WriteLine($"Selected service {attr.Name}.");

                                    if (characteristics.Count > 0)
                                    {
                                        for (int i = 0; i < characteristics.Count; i++)
                                        {
                                            var charToDisplay = new BluetoothLEAttributeDisplay(characteristics[i]);
                                            _characteristics.Add(charToDisplay);
                                            if (!Console.IsInputRedirected) Console.WriteLine($"#{i:00}: {charToDisplay.Name}\t{charToDisplay.Chars}");
                                        }
                                    }
                                    else
                                    {
                                        if (!Console.IsOutputRedirected)
                                            Console.WriteLine("Service don't have any characteristic.");
                                        retVal += 1;
                                    }
                                }
                                else
                                {
                                    if (!Console.IsOutputRedirected)
                                        Console.WriteLine("Error accessing service.");
                                    retVal += 1;
                                }
                            }
                            // Not granted access
                            else
                            {
                                if (!Console.IsOutputRedirected)
                                    Console.WriteLine("Error accessing service.");
                                retVal += 1;
                            }
                        }
                        catch (Exception ex)
                        {
                            if (!Console.IsOutputRedirected)
                                Console.WriteLine($"Restricted service. Can't read characteristics: {ex.Message}");
                            retVal += 1;
                        }
                    }
                    else
                    {
                        if (!Console.IsOutputRedirected)
                            Console.WriteLine("Invalid service name or number");
                        retVal += 1;
                    }
                }
                else
                {
                    if (!Console.IsOutputRedirected)
                        Console.WriteLine("Invalid service name or number");
                    retVal += 1;
                }
            }
            else
            {
                if (!Console.IsOutputRedirected)
                    Console.WriteLine("Nothing to use, no BLE device connected.");
                retVal += 1;
            }

            return retVal;
        }
        private static void Characteristic_ValueChanged(GattCharacteristic sender, GattValueChangedEventArgs args)
        {
            var newValue = Utilities.FormatValue(args.CharacteristicValue, _dataFormat);
            char_value = newValue;
            if (Console.IsInputRedirected) Console.WriteLine($"{newValue}");
            else Console.WriteLine($"Value changed for {sender.Uuid}: {newValue}\n ");
            //if (_notifyCompleteEvent != null)
            //{
            //    _notifyCompleteEvent.Set();
            //    _notifyCompleteEvent = null;
            //}
        }
        #endregion


        #region IBLE Implementation
        public List<BluetoothLEAttributeDisplay> DiscoverServcies(BluetoothLEDevice _selectedDevice)
        {
            return Services;
        }

        public async Task<object> GetGattServiceById(string serviceName)
        {
             _selectedService = _services.FirstOrDefault(s => s.Name.Equals(serviceName));
            await GetCharacteristics(serviceName);
            Console.WriteLine("Characteristics count:"+ _characteristics.Count);
            return _selectedService.Name;
        }

        public async Task<object> OpenDeviceAsync(string deviceName)
        {
            _selectedDevice = await BluetoothLEDevice.FromIdAsync(deviceName);
            return await GetGattServices() ;
        }

        public void CloseDevice()
        {
            Services?.ForEach((s) => { s.service?.Dispose(); });
            Services?.Clear();
            //_characteristics?.Clear();
            _selectedDevice?.Dispose();
        }

        public async Task<object> ListDevices(string param)
        {

            var watcher = DeviceInformation.CreateWatcher(_aqsAllBLEDevices, _requestedBLEProperties, DeviceInformationKind.AssociationEndpoint);
            watcher.Added += (DeviceWatcher sender, DeviceInformation devInfo) =>
            {
                if (_deviceList.FirstOrDefault(d => d.Id.Equals(devInfo.Id) || d.Name.Equals(devInfo.Name)) == null) _deviceList.Add(devInfo);
            };
            watcher.Updated += (_, __) => { }; // We need handler for this event, even an empty!
            //Watch for a device being removed by the watcher
            //watcher.Removed += (DeviceWatcher sender, DeviceInformationUpdate devInfo) =>
            //{
            //    _deviceList.Remove(FindKnownDevice(devInfo.Id));
            //};
            watcher.EnumerationCompleted += (DeviceWatcher sender, object arg) => { sender.Stop(); };
            watcher.Stopped += (DeviceWatcher sender, object arg) => { _deviceList.Clear(); sender.Start(); };
            watcher.Start();

            string cmd = string.Empty;
            bool skipPrompt = false;
            var names = _deviceList.OrderBy(d => d.Name).Where(d => !string.IsNullOrEmpty(d.Name)).Select(d => d.Name).ToList();
            if (string.IsNullOrEmpty(param))
            {
                for (int i = 0; i < names.Count(); i++)
                    Console.WriteLine($"#{i:00}: {names[i]}");
            }
            else if (param.Replace("/", "").ToLower().Equals("w"))
            {
                if (names.Count > 0)
                {
                    // New formatting algorithm for "wide" output; we should avoid tabulations and use spaces only
                    int maxWidth = names.Max(n => n.Length);
                    int columns = Console.WindowWidth / (maxWidth + 5);
                    List<string>[] strColumn = new List<string>[columns];

                    for (int i = 0; i < names.Count; i++)
                    {
                        if (strColumn[i % columns] == null) strColumn[i % columns] = new List<string>();
                        strColumn[i % columns].Add(string.Format("#{0:00}: {1}   ", i, names[i]));
                    }

                    int maxNumColumns = Math.Min(columns, strColumn.Count(l => l != null));

                    for (int i = 0; i < maxNumColumns; i++)
                    {
                        int max = strColumn[i].Max(n => n.Length);
                        for (int j = 0; j < strColumn[i].Count; j++)
                            strColumn[i][j] += new string(' ', max - strColumn[i][j].Length);
                    }

                    for (int j = 0; j < strColumn[0].Count; j++)
                    {
                        string s = "";
                        for (int i = 0; i < maxNumColumns; i++)
                            if (j < strColumn[i].Count) s += strColumn[i][j];
                        Console.WriteLine(s.TrimEnd());
                    }
                }
            }
            watcher.Stop();
            Console.WriteLine("Device list count: " + _deviceList.Count);
            return _deviceList as object;
        }

        public async Task<object> ReadCharacteristic(string param)
        {
            int retVal = 0;

            try
            {
                // Ensure we have access to the device.
                string useName = Utilities.GetIdByNameOrNumber(_characteristics, param);
                var attr = _characteristics.FirstOrDefault(c => c.Name.Equals(useName));
                if (attr != null && attr.characteristic != null)
                {
                    // Read characteristic value
                    GattReadResult result = await attr.characteristic.ReadValueAsync(BluetoothCacheMode.Uncached);

                    if (result.Status == GattCommunicationStatus.Success)
                        Console.WriteLine(Utilities.FormatValue(result.Value, _dataFormat));
                    else
                    {
                        Console.WriteLine($"Read failed: {result.Status}");
                        retVal += 1;
                    }
                }
                else
                {
                    Console.WriteLine($"Invalid characteristic {_selectedCharacteristic.Name}");
                    retVal += 1;
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine($"Restricted service. Can't read characteristics: {ex.Message}");
                retVal += 1;
            }
            return retVal as object;
        }

      
        public async Task<object> SubscribeToCharacteristic(string param)
        {
            int retVal = 0;
            string useName = Utilities.GetIdByNameOrNumber(_characteristics, param);
            var attr = _characteristics.FirstOrDefault(c => c.Name.Equals(useName));
            if (attr != null && attr.characteristic != null )
            {
                // First, check for existing subscription
                
                var status = await attr.characteristic.WriteClientCharacteristicConfigurationDescriptorAsync(GattClientCharacteristicConfigurationDescriptorValue.Notify);
                if (status == GattCommunicationStatus.Success)
                {
                    Console.WriteLine($"Subscribed to characteristic {useName}");
                    attr.characteristic.ValueChanged += Characteristic_ValueChanged;
                    _notifyCompleteEvent.WaitOne(_timeout);
                }
                else
                {
                    if (!Console.IsOutputRedirected)
                        Console.WriteLine($"Can't subscribe to characteristic {useName}");
                    retVal += 1;
                }
                
            }
            else
            {
                if (!Console.IsOutputRedirected)
                    Console.WriteLine($"Invalid characteristic {useName}");
                retVal += 1;
            }
            return char_value as object ;

        }

      
        public void Unsubscribe(string param)
        {
            throw new NotImplementedException();
        }

        public async Task<object> WriteCharacteristic(string param)
        {
            int retVal = 0;
            var buffer = Utilities.FormatData("test message", _dataFormat);
            string useName = Utilities.GetIdByNameOrNumber(_characteristics, param);
            var attr = _characteristics.FirstOrDefault(c => c.Name.Equals(useName));
            if (attr != null && attr.characteristic != null)
            {
                // Write data to characteristic
                GattWriteResult result = await attr.characteristic.WriteValueWithResultAsync(buffer);

                if (result.Status != GattCommunicationStatus.Success)
                {
                    
                    if (!Console.IsOutputRedirected)
                        Console.WriteLine($"Write failed: {result.Status}");
                         retVal += 1;
                }
                else
                {
                    Console.WriteLine("Succesfully write the value");
                }
            }
            else
            {
                if (!Console.IsOutputRedirected)
                    Console.WriteLine($"Invalid characteristic {_selectedCharacteristic.Name}");
                retVal += 1;
            }

            return retVal;
        } 
        #endregion
    }
}
