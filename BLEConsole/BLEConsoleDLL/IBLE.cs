﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Bluetooth;

namespace BLEConsoleDLL
{
    public interface IBLE
    {
        BluetoothLEDevice SelectedDevice { get; set; }
        List<BluetoothLEAttributeDisplay> Services { get; set; }
        Task<object> ListDevices(string param);
        Task<object> OpenDeviceAsync(string deviceName);
        void CloseDevice();
        List<BluetoothLEAttributeDisplay> DiscoverServcies(BluetoothLEDevice _selectedDevice);        
        Task<object> GetGattServiceById(string serviceName);
        Task<object> ReadCharacteristic(string param);
        Task<object> WriteCharacteristic(string param);        
        Task<object> SubscribeToCharacteristic(string param);
        void Unsubscribe(string param);
    }
}
